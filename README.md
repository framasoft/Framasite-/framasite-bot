# Framasite-bot [![pipeline status](https://framagit.org/framasoft/framasite-bot/badges/master/pipeline.svg)](https://framagit.org/framasoft/framasite-bot/commits/master)

Use it with your [Framasite](https://framagit.org/framasoft/framasite) instance.

## Installation

```
git clone https://framagit.org/framasoft/framasite-bot
make
cp /etc/framasite-bot/framasite-bot.conf.template /etc/framasite-bot/framasite-bot.conf
vim /etc/framasite-bot/framasite-bot.conf
systemctl start framasite-bot.service
```

## Update

```
make update
vimdiff /etc/framasite-bot/framasite-bot.conf /etc/framasite-bot/framasite-bot.conf.template
systemctl restart framasite-bot.service
```

## License

Framasite-bot is licensed under the same license than Framasite. See it's [LICENSE](https://framagit.org/framasoft/framasite/blob/master/LICENSE) file.
