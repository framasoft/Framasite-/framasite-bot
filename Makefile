install:
	mkdir /opt/framasite-bot
	cp framasite-bot.pl /opt/framasite-bot
	cp -a etc/ /etc/framasite-bot
	cp /etc/framasite-bot/framasite-bot.conf.dist /etc/framasite-bot/framasite-bot.conf
	cp -a /etc/framasite-bot/templates.dist /etc/framasite-bot/templates
	apt-get -qq -y install build-essential libpq-dev libnetaddr-ip-perl libnet-dns-perl
	cpan Mojolicious Mojo::Pg Mojolicious::Plugin::PgURLHelper Mojolicious::Plugin::Mail File::chown
	cp framasite-bot.service /etc/systemd/system/
	if [ -z ${GITLAB_CI} ]; then systemctl daemon-reload; fi
	if [ -z ${GITLAB_CI} ]; then systemctl enable framasite-bot.service; fi
	@echo "then edit /etc/framasite-bot/framasite-bot.conf"
	@echo "and do: systemctl start framasite-bot.service"

update:
	git pull
	cp framasite-bot.pl /opt/framasite-bot
	rsync -a etc/ /etc/framasite-bot/
	cp framasite-bot.service /etc/systemd/system/
	systemctl daemon-reload
	@echo "\nPlease, compare your framasite-bot.conf to framasite-bot.conf.dist to see if it needs updating"
	@echo "Then do systemctl restart framasite-bot.service"

all: install
