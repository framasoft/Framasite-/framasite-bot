#!/usr/bin/perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use Mojo::Base -base;
use Mojo::Pg;
use Mojo::JSON qw(decode_json);
use Mojo::IOLoop;
use Mojo::Collection 'c';
use Mojolicious;
use Net::DNS;
use NetAddr::IP;
use File::Path qw(remove_tree);
use File::chown;
use Cwd;

use constant {
    STATUS_TODO                  => 10,
    STATUS_WAITING_DNS           => 20,
    STATUS_WAITING               => 30,
    STATUS_FINISHED              => 42,
    STATUS_FAILED                => 1337,
    TYPE_SITE                    => 10,
    TYPE_WIKI                    => 20,
    TYPE_SINGLE                  => 30,
    ACTION_CREATE                => 10,
    ACTION_REDUCE                => 20,
    ACTION_DELETE                => 30,
    DOMAIN_UNKNOWN_ERROR         => 0,
    DOMAIN_REQUESTED             => 10,
    DOMAIN_PAYED                 => 20,
    DOMAIN_VALIDATED             => 30,
    DOMAIN_DNS_CHANGED           => 40,
    DOMAIN_WAITING_CONFIGURATION => 50,
    DOMAIN_VHOST_ADDED           => 60,
    DOMAIN_EXPIRED               => 70,
    DOMAIN_OK                    => 100,
    DOMAIN_REMOVED               => 200,
};

my %statuses = (
    10   => 'STATUS_TODO',
    20   => 'STATUS_WAITING_DNS',
    30   => 'STATUS_WAITING',
    42   => 'STATUS_FINISHED',
    1337 => 'STATUS_FAILED'
);

my %types = (
    10 => 'site',
    20 => 'wiki',
    30 => 'single'
);

# Get config
my $m      = Mojolicious->new();
my $config = $m->plugin(Config => {
        file    => '/etc/framasite-bot/framasite-bot.conf',
        default => {
            certbot_paths => {
                certbot  => '/usr/bin/certbot',
                webroot  => '/var/www/certbot',
                certpath => '/etc/letsencrypt/live/'
            },
            nginx_paths   => {
                nginx     => '/usr/sbin/nginx',
                available => '/etc/nginx/sites-available/',
                enabled   => '/etc/nginx/sites-enabled/'
            },
            mail          => {
                how => 'sendmail'
            },
            send_mail_to_if_problem => '',
            www_user                => 'www-data',
            www_group               => 'www-data',
        }
    }
);

# Create database accessor
$m->plugin('PgURLHelper');
my $pg_url = $m->pg_url($config->{db});
my $pg     = Mojo::Pg->new($pg_url);

# Configure mail plugin
my $mail_config = {
    type     => 'text/plain',
    encoding => 'quoted-printable',
    from     => $config->{mail_sender},
    how      => $config->{mail}->{how}
};
$mail_config->{howargs} = $config->{mail}->{howargs} if (defined $config->{mail}->{howargs});
$m->plugin('Mail' => $mail_config);

# Create name resolver object
my $dns = Net::DNS::Resolver->new;

# Get IPs from the config
my $ipv4 = NetAddr::IP->new($config->{ips}->{ipv4});
my $ipv6 = NetAddr::IP->new6($config->{ips}->{ipv6});

# Add /etc/framasite-bot/ as templates directory
unshift @{$m->renderer->paths}, '/etc/framasite-bot/templates';

# Subscribe to pg pubsub
$pg->pubsub->listen(
    certbot_channel => sub {
        my ($pubsub, $payload) = @_;
        $pg->db->query('SELECT * FROM cert_task WHERE id = ?', ($payload) =>
            sub {
                my ($db, $err, $results) = @_;
                die $err if $err;

                process($results->hash);
            }
        );
        say "Get notif from certbot_channel! $payload";
    }
);

# Periodically recheck the WAITING_FOR_DNS tasks
Mojo::IOLoop->recurring(60 => sub {
    my $loop = shift;
    $pg->db->query('SELECT * FROM cert_task WHERE status = ?', (STATUS_TODO) =>
        sub {
            my ($db, $err, $results) = @_;
            die $err if $err;

            $results->hashes->each(
                sub {
                    my ($e, $num) = @_;
                    process($e);
                }
            );
        }
    );
});

# Periodically recheck the WAITING_FOR_DNS tasks
Mojo::IOLoop->recurring(60 => sub {
    my $loop = shift;
    $pg->db->query('SELECT * FROM cert_task WHERE status = ?', (STATUS_WAITING_DNS) =>
        sub {
            my ($db, $err, $results) = @_;
            die $err if $err;

            $results->hashes->each(
                sub {
                    my ($e, $num) = @_;
                    $e->{status} = STATUS_TODO;
                    process($e);
                }
            );
        }
    );
});

Mojo::IOLoop->recurring(86400 => sub {
    my $loop = shift;
    $pg->db->query("SELECT d.id, d.domain_name, u.email, u.locale FROM domain d JOIN framasite_user u ON u.id = d.user_id
        WHERE expires_at IS NOT NULL
        AND expires_at <= (NOW() - INTERVAL '30 days')
        AND expires_at >  (NOW() - INTERVAL '31 days')" =>
        sub {
            my ($db, $err, $results) = @_;
            die $err if $err;

            $results->hashes->each(
                sub {
                    my ($e, $num) = @_;
                    warn_about_expiration($e, 'month');
                }
            );
        }
    );
    $pg->db->query("SELECT d.id, d.domain_name, u.email, u.locale FROM domain d JOIN framasite_user u ON u.id = d.user_id
        WHERE expires_at IS NOT NULL
        AND expires_at <= (NOW() - INTERVAL '7 days')
        AND expires_at >  (NOW() - INTERVAL '8 days')" =>
        sub {
            my ($db, $err, $results) = @_;
            die $err if $err;

            $results->hashes->each(
                sub {
                    my ($e, $num) = @_;
                    warn_about_expiration($e, 'week');
                }
            );
        }
    );
});

Mojo::IOLoop->start unless Mojo::IOLoop->is_running;

sub process {
    my $task = shift;

    if (defined($task) && $task) {
        # Set task as waiting
        update_status($task->{id}, STATUS_WAITING);

        # Decode the task payload
        $task->{task_data} = decode_json($task->{task_data});

        unless (unauthorized_domains($task)) {
            if ($task->{status} == STATUS_TODO) {
                if ($task->{task_data}->{action} == ACTION_CREATE) {
                    create_cert($task);
                } else {
                    my $create = ($task->{task_data}->{action} == ACTION_REDUCE);

                    revoke_cert($task, !$create);
                    create_cert($task) if ($create);
                }
                if (defined($task->{task_data}->{next})) {
                    $pg->db->query('SELECT * FROM cert_task WHERE id = ?', ($task->{task_data}->{next}) =>
                        sub {
                            my ($db, $err, $results) = @_;
                            die $err if $err;

                            process($results->hash);
                        }
                    );
                }
            } elsif ($task->{status} != STATUS_WAITING) {
                my $todo     = STATUS_TODO;
                my $waiting  = STATUS_WAITING;
                my $received = $statuses{$task->{status}};
                my $msg = <<EOF;
Strange, I should not receive notification for sites in status other than STATUS_TODO ($todo) or STATUS_WAITING ($waiting).
  id:        $task->{id}
  status:    $task->{status} ($received)
  subdomain: $task->{task_data}->{subdomain}
  domains:   @{$task->{task_data}->{names}}
EOF
                warn_and_update_status($msg);
            }
        } else {
            my $msg = <<EOF;
Strange, one of the custom domains matches the unauthorized regex, this should not append.
  id:        $task->{id}
  subdomain: $task->{task_data}->{subdomain}
  domains:   @{$task->{task_data}->{names}}
EOF
            warn_and_update_status($msg, STATUS_FAILED);
        }
    } else {
        my $msg = <<EOF;
Strange, I've been asked to process a task, but didn't receive the task.
EOF
        warn_and_update_status($msg);
    }
}

sub create_cert {
    my $task = shift;
    my $type = $types{$task->{task_data}->{type}};

    # Check if DNS is already good
    my $dns_ok = 0;
    my @warn;
    c(@{$task->{task_data}->{names}})->each(
        sub {
            my ($e, $num) = @_;

            $dns_ok = 1 if check_if_dns_ok($e);
            push @warn, $e unless $dns_ok;
        }
    );

    if ($dns_ok) {
        my @args = (
            $config->{certbot_paths}->{certbot}, 'certonly',
            '--expand',
            '--non-interactive',
            '--rsa-key-size', '4096',
            '--webroot',
            '-w', $config->{certbot_paths}->{webroot},
            '--email', $config->{le_email},
            '--agree-tos',
            '--text',
            '--cert-name', "$task->{task_data}->{subdomain}.$type",
            '-d', join(',', @{$task->{task_data}->{names}})
        );

        if (system(@args) == 0) {
            # Create Nginx vhost
            create_vhost($task);
        } else {
            my $msg = <<EOF;
Something got wrong while trying to issue certificates for @{$task->{task_data}->{names}}
  id:        $task->{id}
  subdomain: $task->{task_data}->{subdomain}
  domains:   @{$task->{task_data}->{names}}
EOF
            warn_and_update_status($msg, $task, STATUS_FAILED, $?);
        }
    } else {
        warn "DNS incorrect for task $task->{id}. Incorrect domains: @warn";
        update_status($task->{id}, STATUS_WAITING_DNS);
    }
}

sub revoke_cert {
    my $task      = shift;
    my $send_mail = shift;
    my $type      = $types{$task->{task_data}->{type}};

    if (-f Mojo::File->new($config->{certbot_paths}->{certpath}, "$task->{task_data}->{subdomain}.$type", "cert.pem")->to_string) {
        my @args = (
            $config->{certbot_paths}->{certbot}, 'revoke', '--delete-after-revoke',
            '--cert-path', Mojo::File->new($config->{certbot_paths}->{certpath}, "$task->{task_data}->{subdomain}.$type", "cert.pem")->to_string,
        );
        my $cert_path = Mojo::File->new($config->{certbot_paths}->{certpath}, "$task->{task_data}->{subdomain}.$type", "cert.pem")->to_string;

        if (system(@args) == 0) {
            remove_tree(Mojo::File->new($config->{certbot_paths}->{certpath}, "$task->{task_data}->{subdomain}.$type"));
            remove_tree(Mojo::File->new($config->{certbot_paths}->{certpath}, '../archive', "$task->{task_data}->{subdomain}.$type"));
            unlink      Mojo::File->new($config->{certbot_paths}->{certpath}, '../renewal', "$task->{task_data}->{subdomain}.$type.conf");
            remove_vhost($task, $send_mail);
        } else {
            my $msg = <<EOF;
Something got wrong while trying to revoke certicate for $task->{task_data}->{subdomain}.$type
  id:        $task->{id}
  subdomain: $task->{task_data}->{subdomain}
  domains:   @{$task->{task_data}->{names}}
  certificate: $cert_path
EOF
            warn_and_update_status($msg, $task, STATUS_FAILED, $?);
        }
    } else {
        my $cert_path = Mojo::File->new($config->{certbot_paths}->{certpath}, "$task->{task_data}->{subdomain}.$type", "cert.pem")->to_string;
        my $msg = <<EOF;
Something got wrong but non-blocking while trying to revoke certicate for $task->{task_data}->{subdomain}.$type
  id:        $task->{id}
  subdomain: $task->{task_data}->{subdomain}
  domains:   @{$task->{task_data}->{names}}
  certificate: $cert_path
  problem:   The certificate $cert_path does not exist. Continue though.
EOF
        warn $msg;
        remove_vhost($task, $send_mail);
    }
}

sub create_vhost {
    my $task = shift;

    my $type     = $types{$task->{task_data}->{type}};
    my $dir      = $config->{root_dirs}->{$type};
    my $c        = $m->build_controller;
    my $template = $c->render_to_string(
        template      => $type,
        format        => 'conf',
        task          => $task,
        root_dir      => $dir,
        certfile      => Mojo::File->new($config->{certbot_paths}->{certpath}, "$task->{task_data}->{subdomain}.$type", "fullchain.pem")->to_string,
        certkey       => Mojo::File->new($config->{certbot_paths}->{certpath}, "$task->{task_data}->{subdomain}.$type", "privkey.pem")->to_string,
    );
    Mojo::File->new($config->{nginx_paths}->{available}, "$task->{task_data}->{subdomain}.$type.conf")->spurt($template);

    # Symlink the new vhost if needed
    my $symlink = (-l Mojo::File->new($config->{nginx_paths}->{enabled},   "$task->{task_data}->{subdomain}.$type.conf")->to_string ||
                  symlink(
                      Mojo::File->new($config->{nginx_paths}->{available}, "$task->{task_data}->{subdomain}.$type.conf")->to_string,
                      Mojo::File->new($config->{nginx_paths}->{enabled},   "$task->{task_data}->{subdomain}.$type.conf")->to_string
                  ));

    if ($symlink) {
        reload_nginx($task, 1);
    } else {
        my $msg = <<EOF;
Something got wrong while trying to symlink $task->{task_data}->{subdomain}.$type.conf
  id:        $task->{id}
  subdomain: $task->{task_data}->{subdomain}
  domains:   @{$task->{task_data}->{names}}
EOF
        warn_and_update_status($msg, $task, STATUS_FAILED);
    }
}

sub remove_vhost {
    my $task      = shift;
    my $send_mail = shift;

    my $type    = $types{$task->{task_data}->{type}};
    my $symlink = Mojo::File->new($config->{nginx_paths}->{enabled}, "$task->{task_data}->{subdomain}.$type.conf")->to_string;
    if (-f $symlink) {
        if (unlink $symlink) {
            reload_nginx($task, $send_mail);
        } else {
            my $msg = <<EOF;
Something got wrong while trying to unlink $symlink
  id:        $task->{id}
  subdomain: $task->{task_data}->{subdomain}
  domains:   @{$task->{task_data}->{names}}
  error:    $!
EOF
            warn_and_update_status($msg, $task, STATUS_FAILED);
        }
    } else {
        reload_nginx($task, $send_mail);
    }
}

sub reload_nginx {
    my $task      = shift;
    my $send_mail = shift;

    # Test nginx configuration
    my @args = ($config->{nginx_paths}->{nginx}, '-t');
    if (system(@args) == 0) {
        # Reload Nginx
        my @args = ($config->{nginx_paths}->{nginx}, '-s', 'reload');
        if (system(@args) == 0) {
            if (create_wiki_symlink($task)) {
                update_domain_status_ok($task);
                update_status($task->{id}, STATUS_FINISHED);
                if ($send_mail) {
                    my $subject  = 'mails/modification_en';
                       $subject  = "mails/modification_$task->{task_data}->{userLocale}" if (-e "/etc/framasite-bot/templates/mails/modification_$task->{task_data}->{userLocale}.subject.ep");
                    my $template = 'mails/modification_en';
                       $template = "mails/modification_$task->{task_data}->{userLocale}" if (-e "/etc/framasite-bot/templates/mails/modification_$task->{task_data}->{userLocale}.mail.ep");

                    my $c        = $m->build_controller;
                    $c->mail(
                        to       => $task->{task_data}->{userEmail},
                        template => $template,
                        format   => 'mail',
                        subject  => $c->render_to_string(template => $template, format => 'subject', task => $task, type => $types{$task->{task_data}->{type}}),
                        task     => $task,
                        ttype    => $types{$task->{task_data}->{type}}
                    );
                }
            }
        } else {
            my $msg = <<EOF;
Something got wrong while reloading Nginx
  id:        $task->{id}
  subdomain: $task->{task_data}->{subdomain}
  domains:   @{$task->{task_data}->{names}}
EOF
            warn_and_update_status($msg, $task, STATUS_FAILED, $?);
        }
    } else {
        my $msg = <<EOF;
Something is wrong in Nginx configuration
  id:        $task->{id}
  subdomain: $task->{task_data}->{subdomain}
  domains:   @{$task->{task_data}->{names}}
EOF
        warn_and_update_status($msg, $task, STATUS_FAILED, $?);
    }
}

sub check_if_dns_ok {
    my $host = shift;

    my $ok     = 1;

    my $reply = $dns->search($host, 'A');
    if ($reply) {
        foreach my $rr ($reply->answer) {
            if ($rr->can("address")) {
                $ok = 0 unless (NetAddr::IP->new($rr->address) == $ipv4);
            } elsif ($rr->can("cname")) {
                $ok = check_if_dns_ok($rr->cname);
            } else {
                $ok = 0;
                last;
            }
        }
    } else {
        $ok = 0;
        warn "A query failed for $host: ", $dns->errorstring, "\n";
    }

    $reply = $dns->search($host, 'AAAA');
    if ($reply) {
        foreach my $rr ($reply->answer) {
            if ($rr->can("address")) {
                $ok = 0 unless (NetAddr::IP->new6($rr->address) == $ipv6);
            } elsif ($rr->can("cname")) {
                $ok = check_if_dns_ok($rr->cname);
            } else {
                $ok = 0;
                last;
            }
        }
    } else {
        $ok = 0;
        warn "AAAA query failed for $host: ", $dns->errorstring, "\n";
    }

    return $ok;
}

sub unauthorized_domains {
    my $task = shift;

    return 0 unless $config->{unauthorized_domains};
    my $unauthorized = c(@{$task->{task_data}->{names}})->map(
        sub {
            my $domain = $_;
            if ($domain =~ m#$config->{unauthorized_domains}#) {
                return $domain;
            } else {
                return undef;
            }
        }
    )->compact;

    return $unauthorized->size;
}

sub warn_about_expiration {
    my $domain = shift;
    my $delay  = shift;

    my $subject  = 'mails/expiration_en';
       $subject  = "mails/expiration_$domain->{locale}" if (-e "/etc/framasite-bot/templates/mails/expiration_$domain->{locale}.subject.ep");
    my $template = 'mails/expiration_en';
       $template = "mails/expiration_$domain->{locale}" if (-e "/etc/framasite-bot/templates/mails/expiration_$domain->{locale}.mail.ep");

    my $c        = $m->build_controller;
    $c->mail(
        to       => $domain->{email},
        template => $template,
        format   => 'mail',
        subject  => $c->render_to_string(template => $template, format => 'subject', domain => $domain, delay => $delay),
        domain   => $domain,
        delay    => $delay,
        site     => $config->{service_url}
    );
}

sub update_status {
    my $id     = shift;
    my $status = shift;
    $pg->db->query('UPDATE cert_task SET status = ?, last_updated = NOW() WHERE id = ?', ($status, $id));
}

sub update_domain_status_ok {
    my $task = shift;

    # Update domain status in the DB
    $pg->db->update('domain' => { status => DOMAIN_OK } => { domain_name => $task->{task_data}->{names} });
}

sub create_wiki_symlink {
    my $task = shift;
    my $type = $types{$task->{task_data}->{type}};

    if ($type eq 'wiki' || $type eq 'single') {
        my $cwd = getcwd;
        chdir($config->{dirs}->{$type});
        my $target = "$task->{task_data}->{subdomain}$task->{task_data}->{suffix}";
        c(@{$task->{task_data}->{names}})->each(
            sub {
                my ($e, $num) = @_;
                unless (-e $e) {
                    my $s = symlink($target, $e);
                    if ($s) {
                        chown({ deref => 0 }, $config->{www_user}, $config->{www_group}, $e);
                    } else {
                        my $msg = <<EOF;
Something got wrong while trying to symlink $task->{task_data}->{subdomain}.$type to $e
  id:        $task->{id}
  subdomain: $task->{task_data}->{subdomain}
  domains:   @{$task->{task_data}->{names}}
EOF
                        warn_and_update_status($msg, $task, STATUS_FAILED);
                        return 0;
                    }
                }
            }
        );
        chdir($cwd);
    }
    return 1;
}

sub warn_and_update_status {
    my $msg    = shift;
    my $task   = shift;
    my $status = shift;
    my $bug    = shift;

    if ($msg && $config->{send_mail_to_if_problem}) {
        my $c = $m->build_controller;
        $c->mail(
            to      => $config->{send_mail_to_if_problem},
            subject => sprintf('[Framasite-bot] Error while processing task'),
            data    => $msg
        );
    }
    warn $msg;
    update_status($task->{id}, $status) if ($task && $status);
}
