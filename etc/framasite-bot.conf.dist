# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
{
    # PostgreSQL DB credentials
    # MANDATORY, no default
    db => {
        host     => 'HOST',
        database => 'DATABASE',
        user     => 'USER',
        pwd      => 'SECRET'
    },

    # Service URL, the URL where your users log in
    # Must be without trailing slash!
    # MANDATORY, no default
    service_url => 'https://example.org',

    # The email that will be used by Let's Encrypt to send notifications to
    # MANDATORY, no default
    le_email => 'tech@example.org',

    # The directories containing the users' content of your different types of site
    # MUST end with a trailing slash
    # MANDATORY, no default
    dirs => {
        # Where the symlinks of Grav are
        site   => '/var/www/example/blog/users/',
        # Where the symlinks of Dokuwiki are
        wiki   => '/var/www/example/wiki/users/',
        # Where PrettyNoemieCMS is
        single => '/var/www/example/single_page/users/',
    },

    # The root document for the nginx vhosts
    # MANDATORY, no default
    root_dirs => {
        # Where the symlinks of Grav are
        # MUST end with a trailing slash
        site   => '/var/www/example/blog/users/',
        # Where Dokuwiki is
        wiki   => '/var/www/example/doku/',
        # Where PrettyNoemieCMS is
        single => '/var/www/example/PrettyNoemieCMS/',
    },

    # IPs addresses of your server
    # Will be used to check if the DNS records of the domains are correctly configured
    # MANDATORY, no default
    ips => {
        ipv4 => '203.0.113.42',
        ipv6 => '2001:DB8::42'
    },

    # Address used as mail sender
    # MANDATORY, no default
    mail_sender => 'no-reply@example.org',

    # If this setting is not empty, a mail will be send to this address if operations have encountered problems
    # Optional, default to empty
    send_mail_to_if_problem => 'tech@example.org',

    # Your webserver user and group. Usually both www-data on Debian
    # Optional, both default to www-data
    #www_user  => 'www-data',
    #www_group => 'www-data',

    # Blacklist of domains, written as a regex
    # Your users' domains will be tested with this regex.
    # If it matches, the processing of the task will fail
    # and will be logged
    # Optional, no default
    #unauthorized_domains => '.*\.example\.(site|wiki)$',

    # Certbot related paths
    # Optional, these are the default values
    #certbot_paths => {
    #    # Path to certbot program
    #    certbot  => '/usr/bin/certbot',
    #    # Directory where certbot will put the challenges
    #    webroot  => '/var/www/certbot',
    #    # Directory where certbot will put the certificates
    #    certpath => '/etc/letsencrypt/live/'
    #},

    # Nginx related paths
    # Optional, these are the default values
    #nginx_paths   => {
    #    # Path to nginx program
    #    nginx     => '/usr/sbin/nginx',
    #    # Directory of nginx's availables virtualhosts
    #    available => '/etc/nginx/sites-available/',
    #    # Directory of nginx's enabled virtualhosts (which are symlinks of available virtualhosts
    #    enabled   => '/etc/nginx/sites-enabled/'
    #},

    # Mail configuration
    # See https://metacpan.org/pod/Mojolicious::Plugin::Mail#EXAMPLES
    # Optional, default to sendmail method with no arguments
    #mail => {
    #    # Valid values are 'sendmail' and 'smtp'
    #    how => 'smtp',
    #    howargs => ['smtp.example.org']
    #},
}
